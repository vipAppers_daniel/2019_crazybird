package com.example.crazybird;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.crazybird.Engine.Game;


public class MainActivity extends Activity {

    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout container = findViewById(R.id.container);
        Button restart = findViewById(R.id.restart);

        game = new Game(this);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!game.isRunning) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }
        });
        container.addView(game);
    }


    @Override
    protected void onPause() {
        super.onPause();
        game.pause();
    }


    @Override
    protected void onResume() {
        super.onResume();
        game.inicia();
        new Thread(game).start();
    }
}
