package com.example.crazybird.Engine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.example.crazybird.Elements.Canos;
import com.example.crazybird.Elements.GameOver;
import com.example.crazybird.Elements.Passaro;
import com.example.crazybird.Elements.Pontuacao;
import com.example.crazybird.Graphics.Tela;
import com.example.crazybird.R;

public class Game extends SurfaceView implements Runnable, View.OnTouchListener {

    public boolean isRunning = true;
    private final SurfaceHolder holder = getHolder();

    private Pontuacao pontuacao;
    private Passaro passaro;
    private Canos canos;
    private Tela tela;
    private Som som;

    private Bitmap background;

    public Game(Context context) {
        super (context);
        tela = new Tela(context);

        inicializaElementos(context);

        setOnTouchListener(this);
    }


    private void inicializaElementos(Context context) {

        this.som = new Som(context);
        this.pontuacao = new Pontuacao();
        this.passaro = new Passaro(tela, context);
        this.canos = new Canos(tela, pontuacao, context, som);

        Bitmap back = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        background = Bitmap.createScaledBitmap(back,
                tela.getLargura(),tela.getAltura(), false);
    }


    @Override
    public void run() {
        while (isRunning){
            if (!holder.getSurface().isValid()) continue; {
                Canvas canvas = holder.lockCanvas();

                canvas.drawBitmap(background, 0,0, null);

                canos.desenhaNo(canvas);
                passaro.desenhaNo(canvas);
                pontuacao.desenhaNo(canvas);

                if (new VerificadorDeColisao(passaro, canos).temColisão()) {
                    som.play(Som.COLISAO);
                    new GameOver(tela).desenhaNo(canvas);
                    isRunning = false;
                }

                passaro.cai();
                canos.move(tela);

                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void pause() {
        this.isRunning = false;
    }

    public void inicia() {
        this.isRunning = true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (isRunning) {
            som.play(Som.PULO);
        }
        passaro.pula();
        return false;
    }
}
