package com.example.crazybird.Engine;

import com.example.crazybird.Elements.Canos;
import com.example.crazybird.Elements.Passaro;

class VerificadorDeColisao {

    private final Passaro passaro;
    private final Canos canos;

    public VerificadorDeColisao(Passaro passaro, Canos canos) {
        this.passaro = passaro;
        this.canos = canos;
    }


    public boolean temColisão() {
        return canos.temColisaoCom(passaro);
    }
}
