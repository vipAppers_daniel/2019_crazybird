package com.example.crazybird.Elements;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.crazybird.Graphics.Cores;
import com.example.crazybird.Graphics.Tela;

public class GameOver {

    private static final Paint VERMELHO = Cores.getCorDoGameOver();
    private final Tela tela;

    public GameOver(Tela tela) {
        this.tela = tela;
    }


    public void desenhaNo(Canvas canvas) {
        String gameOver = "Game Over";
        int centroHorizontal = centralizaTexto(gameOver);

        canvas.drawText(gameOver, centroHorizontal, tela.getAltura()/2, VERMELHO);
    }



    private int centralizaTexto(String text) {
        Rect limiteDoTexto = new Rect();
        VERMELHO.getTextBounds(text, 0, text.length(), limiteDoTexto);

        return tela.getLargura()/2 - (limiteDoTexto.right - limiteDoTexto.left)/2;
    }
}
