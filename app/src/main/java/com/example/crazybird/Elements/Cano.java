package com.example.crazybird.Elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.crazybird.Graphics.Cores;
import com.example.crazybird.Graphics.Tela;
import com.example.crazybird.R;

public class Cano {

    private Tela tela;

    private static final int TAMANHO_DO_CANO = 400;
    private static final int LARGURA_DO_CANO = 175;

    private static final Paint CINZA = Cores.getCorDoCanoCinza();
    private static final Paint VERDE = Cores.getCorDoCanoVerde();

    private int alturaDoCanoInferior;
    private int alturaDoCanoSuperior;

    private int position;

    private int velocidade = 5;
    private int nivelDificuldade = 10;

    private Pontuacao pontuacao;

    private Bitmap canoSuperior;
    private Bitmap canoInferior;

    public Cano(Tela tela, int position, Pontuacao pontuacao, Context context){

        this.pontuacao = pontuacao;

        this.tela = tela;
        this.position = position;
        this.alturaDoCanoInferior = tela.getAltura() - TAMANHO_DO_CANO - valorAleatorio();
        this.alturaDoCanoSuperior = TAMANHO_DO_CANO + valorAleatorio();

        Bitmap superior = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.cano_superior);
        this.canoSuperior = Bitmap.createScaledBitmap(superior, LARGURA_DO_CANO,
                alturaDoCanoSuperior , false);

        Bitmap inferior = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.cano_inferior);
        this.canoInferior = Bitmap.createScaledBitmap(inferior, LARGURA_DO_CANO,
                tela.getAltura() - alturaDoCanoInferior , false);
    }


    public void desenhaNo(Canvas canvas) {
        desenhaCanoInferiorNo(canvas);
        desenhaCanoSuperiorNo(canvas);
    }


    private void desenhaCanoSuperiorNo(Canvas canvas) {
        canvas.drawRect(position, 0, position + LARGURA_DO_CANO, alturaDoCanoSuperior, VERDE);
        canvas.drawBitmap(canoSuperior, position,0, null);
    }


    private void desenhaCanoInferiorNo(Canvas canvas) {
        canvas.drawRect(position, alturaDoCanoInferior, position + LARGURA_DO_CANO, tela.getAltura(), VERDE);
        canvas.drawBitmap(canoInferior, position, alturaDoCanoInferior,null);
    }


    public void move() {

        position -= velocidade;

        int pontos = pontuacao.getPontos();

        if (pontos > nivelDificuldade) {
            velocidade += 1;
            nivelDificuldade += 10;
        }
    }

    private int valorAleatorio() {
        return (int) (Math.random() * 250);
    }

    public boolean saiuDaTela() {
        return position + LARGURA_DO_CANO < 0;
    }

    public int getPosition() {
        return position;
    }


    public boolean temColisaoVerticalCom(Passaro passaro) {
        return passaro.getAltura() - passaro.RAIO < this.alturaDoCanoSuperior ||
                passaro.getAltura() + passaro.RAIO > this.alturaDoCanoInferior;
    }


    public boolean temColisaoHorizontalCom(Passaro passaro) {
        if (position + LARGURA_DO_CANO < 30) {
            return false;
        }
        else {
            return this.position - passaro.X < passaro.RAIO;
        }
    }

}
