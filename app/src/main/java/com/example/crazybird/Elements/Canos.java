package com.example.crazybird.Elements;

import android.content.Context;
import android.graphics.Canvas;

import com.example.crazybird.Engine.Som;
import com.example.crazybird.Graphics.Tela;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Canos {

    private List<Cano> canos = new ArrayList<>();

    private static final int QUANTIDADE_DE_CANOS = 5;
    private static final int DISTANCIA_ENTRE_CANOS = 330;

    private Som som;
    private Context context;
    private Pontuacao pontuacao;

    public Canos (Tela tela, Pontuacao pontuacao, Context context, Som som) {

        this.pontuacao = pontuacao;
        this.context = context;
        this.som = som;

        int positionInicial = 800;

        for (int i = 0; i < QUANTIDADE_DE_CANOS; i ++) {
            canos.add(new Cano(tela, positionInicial, pontuacao, context));
            positionInicial += DISTANCIA_ENTRE_CANOS;
        }
    }

    public void desenhaNo(Canvas canvas) {
        for (Cano cano : canos) {
            cano.desenhaNo(canvas);
        }
    }


    public void move(Tela tela) {

        ListIterator<Cano> listaDeCanos = canos.listIterator();

        while (listaDeCanos.hasNext()) {
            Cano cano = listaDeCanos.next();
            cano.move();

            if(cano.saiuDaTela()) {

                som.play(Som.PONTOS);

                pontuacao.aumenta();
                listaDeCanos.remove();

                Cano outroCano = new Cano (tela, getMaximo() + DISTANCIA_ENTRE_CANOS, pontuacao, context);
                listaDeCanos.add(outroCano);
            }
        }
    }

    private int getMaximo() {
        int maximo = 0;
        for (Cano cano : canos) {
            maximo = Math.max(cano.getPosition(), maximo);
        }
        return maximo;
    }


    public boolean temColisaoCom(Passaro passaro) {
        for (Cano cano : canos) {
            if (cano.temColisaoHorizontalCom(passaro) && cano.temColisaoVerticalCom(passaro)) {
                return true;
            }
        }
        return false;
    }
}
