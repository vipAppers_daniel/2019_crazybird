package com.example.crazybird.Elements;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.crazybird.Graphics.Cores;

public class Pontuacao {

    private static final Paint BRANCO = Cores.getCorDaPontuacao();
    private int pontos = 0;

    public void aumenta() {
        pontos ++;
    }

    public void desenhaNo(Canvas canvas) {
        canvas.drawText(String.valueOf(pontos), 100, 200, BRANCO);
    }

    public int getPontos() {
        return pontos;
    }
}
