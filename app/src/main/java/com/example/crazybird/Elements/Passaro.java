package com.example.crazybird.Elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.crazybird.Graphics.Cores;
import com.example.crazybird.Graphics.Tela;
import com.example.crazybird.R;

public class Passaro {

    private static final Paint VERMELHO = Cores.getCorDoPassaro();

    private int altura;

    int X = 130;
    int RAIO = 100;

    private Tela tela;

    private Bitmap passaro;


    public Passaro(Tela tela, Context context) {
        this.tela = tela;
        this.altura = tela.getAltura() / 2;

        Bitmap bp = BitmapFactory.decodeResource(context.getResources(), R.drawable.passaro);
        this.passaro = Bitmap.createScaledBitmap(bp, RAIO*2, RAIO*2 , false);
    }

    public void desenhaNo(Canvas canvas) {
        canvas.drawCircle(X, altura, RAIO, VERMELHO);
        canvas.drawBitmap(passaro, X-RAIO, altura-RAIO, null );
    }

    public void cai() {
        boolean chegouNoChao = altura + RAIO > tela.getAltura();

        if (!chegouNoChao) {
            this.altura += 5;
        }
    }

    public void pula() {

        if (altura - RAIO > 0) {
            this.altura -= 150;
        }
    }

    public int getAltura() {
        return this.altura;
    }
}
