package com.example.crazybird.Graphics;

import android.graphics.Paint;
import android.graphics.Typeface;

public class Cores {

    public static Paint getCorDoPassaro() {
        Paint vermelho = new Paint();
        vermelho.setColor(0x10FF0000);
        return vermelho;
    }

    public static Paint getCorDoCanoCinza() {
        Paint cinza = new Paint();
        cinza.setColor(0xFF777777);
        return cinza;
    }

    public static Paint getCorDoCanoVerde() {
        Paint verde = new Paint();
        verde.setColor(0x2000FF00);
        return verde;
    }

    public static Paint getCorDaPontuacao() {
        Paint branco = new Paint();
        branco.setColor(0xFFFFFFFF);
        branco.setTextSize(120);
        branco.setTypeface(Typeface.DEFAULT_BOLD);
        branco.setShadowLayer(3, 5, 5, 0xFF000000);

        return branco;
    }


    public static Paint getCorDoGameOver() {
        Paint vermelhoGameOver = new Paint();
        vermelhoGameOver.setColor(0xFFFF0000);
        vermelhoGameOver.setTextSize(120);
        vermelhoGameOver.setTypeface(Typeface.DEFAULT_BOLD);
        vermelhoGameOver.setShadowLayer(3, 5, 5, 0xFF000000);

        return vermelhoGameOver;
    }
}
